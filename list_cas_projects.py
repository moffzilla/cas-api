import json
import requests

# Collects API URL and Token
api_token = input ("Enter your API Token:")
api_url_base = input ("Enter your API Token:") 

headers = {'Content-Type': 'application/json',
           'Accept': 'application/json'}

data = { 'refreshToken': format(api_token)  }

# Collects the Bearer Token

def get_authorization_bearer():

    api_url = format(api_url_base)
    response = requests.post(api_url, headers=headers, json=data)

    if response.status_code == 200:
        bearer_code = json.loads(response.content)
        #print (bearer_code)
        return bearer_code 
    else:
        return None

get_authorization_bearer = get_authorization_bearer()

# Prints Bearer Token call response

if get_authorization_bearer is not None:
    print("Here's your info:" )
#    print(json.dumps(get_authorization_bearer))
    print(get_authorization_bearer)

else:
    print('[!] Request Failed')


